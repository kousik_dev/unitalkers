window.onload=function()
{
	var socket = io("https://unitalker.herokuapp.com");
	
	$(".send-btn").click(function(){
		
			var html=renderMyMsg($(".write-msg").html());
			$(".chatting").append(html);
			
			socket.emit('chat message', $('.write-msg').html());
			
			$('.write-msg').html("");
		})
		
	//on receive any other message
	
	socket.on("message received", function(msg){
		
			var html=renderOtherMsg(msg);
			
			$(".chatting").append(html);
		
	  });
	
	//on receive any other message
}
