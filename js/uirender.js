function renderMyMsg(msg)
{
	var html="<div class=\"message-from-me\">";
						
	html=html+"<div class=\"mymsg left\">";
	html=html+"<div class=\"chat-image\">";
	html=html+"<img style=\"width:100%;height:100%;border-radius:50%\" src=\"../images/profilepic.jpg\">";
	html=html+"</div>";
	html=html+"</div>";
						
	html=html+"<div class=\"mymsg-right right\">";
	html=html+msg;
	html=html+"</div>";
	html=html+"</div>";
	
	return html;
}

function renderOtherMsg(msg)
{
	html="<div class=\"message-from-others\">";
	
	html=html+"<div class=\"left\">";
	html=html+"<div class=\"chat-image\">";
	html=html+"<img style=\"width:100%;height:100%;border-radius:50%\" src=\"../images/profilepic.jpg\">";
	html=html+"</div>";
	html=html+"</div>";
						
	html=html+"<div class=\"right\">";
	html=html+msg;
	html=html+"</div>";
	html=html+"</div>";
	
	return html;
}
